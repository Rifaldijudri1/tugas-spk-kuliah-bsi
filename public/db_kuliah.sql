/*
 Navicat Premium Data Transfer

 Source Server         : localhost_db
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : localhost:3306
 Source Schema         : db_kuliah

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 16/11/2023 14:03:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bobot
-- ----------------------------
DROP TABLE IF EXISTS `bobot`;
CREATE TABLE `bobot`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `selisih` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nilai` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bobot
-- ----------------------------
INSERT INTO `bobot` VALUES (1, '2577a307-5ccc-4ac9-9e8a-5d3831011f49', '0', '9', 'Tidak ada selisih (kompetensi sesuai dengan  yang dibutuhkan  ', '2023-11-14 23:44:14', '2023-11-14 23:44:16');
INSERT INTO `bobot` VALUES (2, '2577a307-5ccc-4ac9-9e8a-5d3831011f50', '1', '8', 'Kompetensi kelebihan 1  tingkat/level  ', '2023-11-14 23:44:44', '2023-11-14 23:44:47');
INSERT INTO `bobot` VALUES (3, '2577a307-5ccc-4ac9-9e8a-5d3831011f51', '-1', '7', 'Kompetensi kekurangan 1  tingkat/level  ', '2023-11-14 23:45:20', '2023-11-14 23:45:22');
INSERT INTO `bobot` VALUES (4, '2577a307-5ccc-4ac9-9e8a-5d3831011f52', '2', '6', 'Kompetensi kelebihan 2  tingkat/level  ', '2023-11-14 23:45:47', '2023-11-14 23:45:50');
INSERT INTO `bobot` VALUES (5, '2577a307-5ccc-4ac9-9e8a-5d3831011f53', '-2', '5', 'Kompetensi kekurangan 2  tingkat/level  ', '2023-11-14 23:46:12', '2023-11-14 23:46:16');
INSERT INTO `bobot` VALUES (6, '2577a307-5ccc-4ac9-9e8a-5d3831011f54', '3', '4', 'Kompetensi kelebihan 3  tingkat/level ', '2023-11-14 23:46:37', '2023-11-14 23:46:40');
INSERT INTO `bobot` VALUES (7, '2577a307-5ccc-4ac9-9e8a-5d3831011f55', '-3', '3', 'Kompetensi kekurangan 3  tingkat/level  ', '2023-11-14 23:47:09', '2023-11-14 23:47:11');
INSERT INTO `bobot` VALUES (8, '2577a307-5ccc-4ac9-9e8a-5d3831011f56', '4', '2', 'Kompetensi kelebihan 4  tingkat/level  ', '2023-11-14 23:47:38', '2023-11-14 23:47:40');
INSERT INTO `bobot` VALUES (9, '2577a307-5ccc-4ac9-9e8a-5d3831011f57', '-4', '1', 'Kompetensi kekurangan 4  tingkat/level  ', '2023-11-14 23:47:58', '2023-11-14 23:48:00');

-- ----------------------------
-- Table structure for kriteria
-- ----------------------------
DROP TABLE IF EXISTS `kriteria`;
CREATE TABLE `kriteria`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kriteria
-- ----------------------------
INSERT INTO `kriteria` VALUES (1, '2577a307-5ccc-4ac9-9e8a-5d3831011A79', 'ASPEK AKADEMIK', 'Active', '2023-11-14 23:49:31', '2023-11-14 23:49:34');
INSERT INTO `kriteria` VALUES (2, '2577a307-5ccc-4ac9-9e8a-5d3831011A80', 'ASPEK NON AKADEMIK', 'Active', '2023-11-14 23:49:57', '2023-11-14 23:50:00');

-- ----------------------------
-- Table structure for mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE `mahasiswa`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nim` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `kelas` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `A1` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `A2` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `A3` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `A4` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `A5` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `A6` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nilai_akhir` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mahasiswa
-- ----------------------------
INSERT INTO `mahasiswa` VALUES (1, '6a98db4b-d4b1-40e8-be7a-8b3d552c5471', 'Rifaldi', '19232495', '19.7AA.07', '2', '3', '4', '3', '5', '5', NULL, '2023-11-15 16:08:22', '2023-11-15 16:08:22');
INSERT INTO `mahasiswa` VALUES (2, '5bddb036-31f3-4757-b03c-f48df8fa6a19', 'Riko', '19232353', '19.7AA.07', '3', '3', '4', '3', '4', '5', NULL, '2023-11-16 04:29:19', '2023-11-16 04:29:19');
INSERT INTO `mahasiswa` VALUES (3, '377df5d8-e940-46e3-9901-8dec26479a7d', 'Rosidah', '19232474', '19.7AA.07', '5', '1', '4', '3', '4', '2', NULL, '2023-11-16 05:49:15', '2023-11-16 05:49:15');

-- ----------------------------
-- Table structure for nilai_mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `nilai_mahasiswa`;
CREATE TABLE `nilai_mahasiswa`  (
  `id` int(0) NOT NULL,
  `uid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_mahasiswa` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_sub_kriteria` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_kriteria` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nilai` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `hasil_akhir` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sub_kriteria
-- ----------------------------
DROP TABLE IF EXISTS `sub_kriteria`;
CREATE TABLE `sub_kriteria`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_kriteria` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `kode` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nilai` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `faktor` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sub_kriteria
-- ----------------------------
INSERT INTO `sub_kriteria` VALUES (1, '1077a307-5ccc-4ac9-9e8a-5d3831011S79', '2577a307-5ccc-4ac9-9e8a-5d3831011A79', 'IPK', 'A1', '4', 'Core Factor', 'Active', '2023-11-14 23:54:22', '2023-11-14 23:54:26');
INSERT INTO `sub_kriteria` VALUES (2, '1077a307-5ccc-4ac9-9e8a-5d3831011B29', '2577a307-5ccc-4ac9-9e8a-5d3831011A79', 'Sertifikat Kompetensi', 'A2', '4', 'Core Factor', 'Active', '2023-11-14 23:55:22', '2023-11-14 23:55:25');
INSERT INTO `sub_kriteria` VALUES (3, '1077a307-5ccc-4ac9-9e8a-5d3831011J29P', '2577a307-5ccc-4ac9-9e8a-5d3831011A79', 'TOEFL', 'A3', '4', 'Secondary Factor', 'Active', '2023-11-14 23:56:06', '2023-11-14 23:56:09');
INSERT INTO `sub_kriteria` VALUES (4, 'x577a307-5ccc-4ac9-9e8a-5d3831011m80', '2577a307-5ccc-4ac9-9e8a-5d3831011A80', 'Karya Tulis ', 'A4', '3', 'Core Factor', 'Active', '2023-11-14 23:57:11', '2023-11-14 23:57:16');
INSERT INTO `sub_kriteria` VALUES (5, 'x577a307-5ccc-4ac9-9e8a-5d3831011x20', '2577a307-5ccc-4ac9-9e8a-5d3831011A80', 'UKM', 'A5', '3', 'Secondary Factor', 'Active', '2023-11-14 23:58:00', '2023-11-14 23:58:04');
INSERT INTO `sub_kriteria` VALUES (6, 'x577a307-5ccc-4ac9-9e8a-5d3851011x80', '2577a307-5ccc-4ac9-9e8a-5d3831011A80', 'Prestasi Kompetisi', 'A6', '3', 'Core Factor', 'Active', '2023-11-14 23:58:48', '2023-11-14 23:58:51');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `uid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama_lengkap` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '432934d7-6be1-4915-902f-ec83ab7f6ABC', 'Guru ', 'guru', '$2y$10$gyRQziNzXuWmnfxseTTp6OSjFjU/3dHIbtflIuwUMw9HBRR06k4ae', 'Super', 'Active', '2023-11-15 19:11:03', '2023-11-15 19:11:05');

SET FOREIGN_KEY_CHECKS = 1;
