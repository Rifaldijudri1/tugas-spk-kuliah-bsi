@extends('templates.header')
@push('style')
@endpush
@section('content')


<div class="container-fluid" id="container-wrapper">
	<div class="row mb-3">
		<div class="col-lg-12">
			<div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-white">Sub Kriteria</h6>
                    
                </div>
                <div class="table-responsive p-3">
                    
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                    	<thead class="thead-light">
                        	<tr>
                        	    <th width="5%">No</th>
                                <th>Kriteria</th>
                                <th>Nama</th>
                                <th>Kode</th>
                                <th>Nilai</th>
                                <th>Faktor</th>
                                <th>&nbsp;</th>

                        	</tr>
                    	</thead>
                        @php
                            $no=1;
                        @endphp
                    	<tbody>
                        	 @forelse ($sub_kriteria as $sub_kriteria)
                                    <tr class="bg">
                                        <td>{{$no++}}</td>
                                       
                                        <td>
                                          {{$sub_kriteria->kriteria->nama}}
                                        </td>
                                        <td>
                                          {{$sub_kriteria->nama}}
                                        </td>

                                         <td>
                                          {{$sub_kriteria->kode}}
                                        </td>

                                         <td>
                                          {{$sub_kriteria->nilai}}
                                        </td>

                                         <td>
                                          {{$sub_kriteria->faktor}}
                                        </td>
                                        
                                        
                                        @if($sub_kriteria->status == 'Active')
                                        <td><span class="badge badge-success">{{$sub_kriteria->status}}</span></td>
                                        @else
                                        <td><span class="badge badge-danger">{{$sub_kriteria->status}}</span></td>  
                                        @endif
                                        
                                       
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse
                              
                        
                    	</tbody>
                  	</table>
                </div>
            </div>
        </div>
    </div>
</div>



    

     

@endsection