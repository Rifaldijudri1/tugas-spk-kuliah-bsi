@extends('templates.header')
@push('style')
@endpush
@section('content')


<div class="container-fluid" id="container-wrapper">
	<div class="row mb-3">
		<div class="col-lg-12">
			<div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-white">Perhitungan Nilai</h6>
                    
                </div>
                <div class="table-responsive p-3">
                    
                    <table class="table align-items-center  table-hover" border='1'>
                    	
                            <tr style="background: rgba(181, 181, 181, 0.852);">
                                <td colspan="12" align="center">Nilai GAP</td>
                                
                            </tr>
                            <tr>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NO.</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NAMA</td>
                                <td align="center" colspan="3" style="background: rgb(117, 247, 117);">ASPEK AKADEMIK</td>
                                
                                <td align="center" colspan="3" style="background: rgb(117, 247, 117);">ASPEK NON AKADEMIK</td>
                              
                            </tr>
                            <tr>
                                <td style="background: rgb(117, 247, 117);">A1</td>
                                <td style="background: rgb(117, 247, 117);">A2</td>
                                <td style="background: rgb(117, 247, 117);">A3</td>
                                <td style="background: rgb(117, 247, 117);">A4</td>
                                <td style="background: rgb(117, 247, 117);">A5</td>
                                <td style="background: rgb(117, 247, 117);">A6</td>
                            </tr>
                             @php
                                 $no=1;
                             @endphp
                           
                             @forelse ($mahasiswa as $mahasiswa)
                                    <tr class="bg">
                                        <td align="center">{{$no++}}</td>
                                        <td align="center">
                                          {{$mahasiswa->nama}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A1}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A2}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A3}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A4}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A5}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A6}}
                                        </td>
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse
                        
                            
                            <tr>
                                <td colspan="2" align="center" style="background:rgb(172, 172, 172); color:white;">Nilai Standar</td>
                               
                                <td>{{$nilaiA1}}</td>
                                <td>{{$nilaiA2}}</td>
                                <td>{{$nilaiA3}}</td>
                                <td>{{$nilaiA4}}</td>
                                <td>{{$nilaiA5}}</td>
                                <td>{{$nilaiA6}}</td>
                            </tr>

                             @php
                                 $no2=1;
                             @endphp
                           
                             @forelse ($mahasiswa2 as $m)
                                    <tr class="bg">
                                        <td align="center">{{$no2++}}</td>
                                        <td align="center">
                                          {{$m->nama}}
                                        </td>
                                        <td>
                                            {{ $m->A1 - $nilaiA1 }}
                                        </td>
                                        <td>
                                            {{ $m->A2 - $nilaiA2 }}
                                        </td>
                                        <td>
                                            {{ $m->A3 - $nilaiA3 }}
                                        </td>
                                        <td>
                                            {{ $m->A4 - $nilaiA4 }}
                                        </td>
                                        <td>
                                            {{ $m->A5 - $nilaiA5 }}
                                        </td>
                                        <td>
                                            {{ $m->A6 - $nilaiA6 }}
                                        </td>
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse


                            
                           
                        
                  	</table>
                </div>


                {{-- Konversi Nilai Ke Bobot --}}


                <div class="table-responsive p-3">
                    
                    <table class="table align-items-center  table-hover" border='1'>
                    	
                            <tr style="background: rgba(181, 181, 181, 0.852);">
                                <td colspan="12" align="center">Konversi Nilai Ke Bobot</td>
                                
                            </tr>
                            <tr>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NO.</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NAMA</td>
                                <td align="center" colspan="3" style="background: rgb(117, 247, 117);">ASPEK AKADEMIK</td>
                                
                                <td align="center" colspan="3" style="background: rgb(117, 247, 117);">ASPEK NON AKADEMIK</td>
                              
                            </tr>
                            <tr>
                                <td style="background: rgb(117, 247, 117);">A1</td>
                                <td style="background: rgb(117, 247, 117);">A2</td>
                                <td style="background: rgb(117, 247, 117);">A3</td>
                                <td style="background: rgb(117, 247, 117);">A4</td>
                                <td style="background: rgb(117, 247, 117);">A5</td>
                                <td style="background: rgb(117, 247, 117);">A6</td>
                            </tr>
                             @php
                                 $no_bobot=1;
                             @endphp
                           
                             @forelse ($mahasiswa_bobot as $m_bobot)
                                    <tr class="bg">
                                        <td align="center">{{$no_bobot++}}</td>
                                        <td align="center">
                                          {{$m_bobot->nama}}
                                        </td>
                                        <td>
                                            {{ $m_bobot->A1 - $nilaiA1 }}
                                        </td>
                                        <td>
                                            {{ $m_bobot->A2 - $nilaiA2 }}
                                        </td>
                                        <td>
                                            {{ $m_bobot->A3 - $nilaiA3 }}
                                        </td>
                                        <td>
                                            {{ $m_bobot->A4 - $nilaiA4 }}
                                        </td>
                                        <td>
                                            {{ $m_bobot->A5 - $nilaiA5 }}
                                        </td>
                                        <td>
                                            {{ $m_bobot->A6 - $nilaiA6 }}
                                        </td>
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse
                        
                            
                            <tr>
                                <td colspan="12" align="center" style="background:rgb(172, 172, 172); color:white;">Konversi Nilai Ke Bobot</td>
                            </tr>

                             @php
                                 $no_bobot=1;

                                  $nilaiBobotMapping = [
                                        0 => 9,
                                        1 => 8,
                                        -1 => 7,
                                        2 => 6,
                                        -2 => 5,
                                        3 => 4,
                                        -3 => 3,
                                        4 => 2,
                                        -4 => 1,
                                    ];
                             @endphp
                           
                             @forelse ($mahasiswa_bobot2 as $m_bobot)
                                    <tr class="bg">
                                        <td align="center">{{$no_bobot++}}</td>
                                        <td align="center">
                                          {{$m_bobot->nama}}
                                        </td>
                                        <td>
                                             @php
                                                $differenceA1 = $m_bobot->A1 - $nilaiA1;
                                                $nilaiBobotA1 = isset($nilaiBobotMapping[$differenceA1]) ? $nilaiBobotMapping[$differenceA1] : '';
                                                echo $nilaiBobotA1;
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                                $differenceA2 = $m_bobot->A2 - $nilaiA2;
                                                $nilaiBobotA2 = isset($nilaiBobotMapping[$differenceA2]) ? $nilaiBobotMapping[$differenceA2] : '';
                                                echo $nilaiBobotA2;
                                            @endphp
                                            
                                        </td>
                                        <td>
                                            @php
                                                $differenceA3 = $m_bobot->A3 - $nilaiA3;
                                                $nilaiBobotA3 = isset($nilaiBobotMapping[$differenceA3]) ? $nilaiBobotMapping[$differenceA3] : '';
                                                echo $nilaiBobotA3;
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                                $differenceA4 = $m_bobot->A4 - $nilaiA4;
                                                $nilaiBobotA4 = isset($nilaiBobotMapping[$differenceA4]) ? $nilaiBobotMapping[$differenceA4] : '';
                                                echo $nilaiBobotA4;
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                                $differenceA5 = $m_bobot->A5 - $nilaiA5;
                                                $nilaiBobotA5 = isset($nilaiBobotMapping[$differenceA5]) ? $nilaiBobotMapping[$differenceA5] : '';
                                                echo $nilaiBobotA5;
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                                $differenceA6 = $m_bobot->A6 - $nilaiA6;
                                                $nilaiBobotA6 = isset($nilaiBobotMapping[$differenceA6]) ? $nilaiBobotMapping[$differenceA6] : '';
                                                echo $nilaiBobotA6;
                                            @endphp
                                        </td>
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse

                        
                  	</table>
                </div>


                {{-- Konversi Core Factor dan Secondary Factor --}}


                 <div class="table-responsive p-3">
                    
                    <table class="table align-items-center  table-hover" border='1'>
                    	
                            {{-- <tr style="background: rgba(181, 181, 181, 0.852);">
                                <td colspan="12" align="center">Konversi Core Factor dan Secondary Factor</td>     
                            </tr> --}}
                            <tr>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NO.</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NAMA</td>
                                <td align="center" colspan="3" style="background: rgb(117, 247, 117);">ASPEK AKADEMIK</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">CF</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">SF</td>
                                
                              
                            </tr>
                            <tr>
                                <td style="background: rgb(117, 247, 117);">A1</td>
                                <td style="background: rgb(117, 247, 117);">A2</td>
                                <td style="background: rgb(117, 247, 117);">A3</td>
                            </tr>
                             
                             @php
                                 $no_bobot=1;

                                  $nilaiBobotMapping = [
                                        0 => 9,
                                        1 => 8,
                                        -1 => 7,
                                        2 => 6,
                                        -2 => 5,
                                        3 => 4,
                                        -3 => 3,
                                        4 => 2,
                                        -4 => 1,
                                    ];
                             @endphp
                           
                             @forelse ($mahasiswa_core as $m_core)
                                    <tr class="bg">
                                        <td align="center">{{$no_bobot++}}</td>
                                        <td align="center">
                                          {{$m_core->nama}}
                                        </td>
                                        <td>
                                             @php
                                                $differenceA1 = $m_core->A1 - $nilaiA1;
                                                $nilaiBobotA1 = isset($nilaiBobotMapping[$differenceA1]) ? $nilaiBobotMapping[$differenceA1] : '';
                                                echo $nilaiBobotA1;
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                                $differenceA2 = $m_core->A2 - $nilaiA2;
                                                $nilaiBobotA2 = isset($nilaiBobotMapping[$differenceA2]) ? $nilaiBobotMapping[$differenceA2] : '';
                                                echo $nilaiBobotA2;
                                            @endphp
                                            
                                        </td>
                                        <td>
                                            @php
                                                $differenceA3 = $m_core->A3 - $nilaiA3;
                                                $nilaiBobotA3 = isset($nilaiBobotMapping[$differenceA3]) ? $nilaiBobotMapping[$differenceA3] : '';
                                                echo $nilaiBobotA3;
                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php

                                             $averageA1A2 = ($nilaiBobotA1 + $nilaiBobotA2) / 2;
                                             echo $averageA1A2;

                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php
                                                $differenceA3 = $m_core->A3 - $nilaiA3;
                                                $nilaiBobotA3 = isset($nilaiBobotMapping[$differenceA3]) ? $nilaiBobotMapping[$differenceA3] : '';
                                                echo $nilaiBobotA3;
                                            @endphp
                                        </td>
                                       
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse

                        
                  	</table>
                </div>


                <div class="table-responsive p-3" style="margin-top:-32px; ">
                    
                    <table class="table align-items-center  table-hover" border='1'>
                    	
                            <tr style="background: rgba(181, 181, 181, 0.852);">
                                <td colspan="12" align="center">Konversi Core Factor dan Secondary Factor</td>
                                
                            </tr>
                            <tr>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NO.</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NAMA</td>
                                <td align="center" colspan="3" style="background: rgb(117, 247, 117);">ASPEK NON AKADEMIK</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">CF</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">SF</td>
                                
                              
                            </tr>
                            <tr>
                                <td style="background: rgb(117, 247, 117);">A4</td>
                                <td style="background: rgb(117, 247, 117);">A5</td>
                                <td style="background: rgb(117, 247, 117);">A6</td>
                            </tr>
                             
                             @php
                                 $no_bobot=1;

                                  $nilaiBobotMapping = [
                                        0 => 9,
                                        1 => 8,
                                        -1 => 7,
                                        2 => 6,
                                        -2 => 5,
                                        3 => 4,
                                        -3 => 3,
                                        4 => 2,
                                        -4 => 1,
                                    ];
                             @endphp
                           
                             @forelse ($mahasiswa_core as $m_core)
                                    <tr class="bg">
                                        <td align="center">{{$no_bobot++}}</td>
                                        <td align="center">
                                          {{$m_core->nama}}
                                        </td>
                                        <td>
                                             @php
                                                $differenceA4 = $m_core->A4 - $nilaiA4;
                                                $nilaiBobotA4 = isset($nilaiBobotMapping[$differenceA4]) ? $nilaiBobotMapping[$differenceA4] : '';
                                                echo $nilaiBobotA4;
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                                $differenceA5 = $m_core->A5 - $nilaiA5;
                                                $nilaiBobotA5 = isset($nilaiBobotMapping[$differenceA5]) ? $nilaiBobotMapping[$differenceA5] : '';
                                                echo $nilaiBobotA5;
                                            @endphp
                                            
                                        </td>
                                        <td>
                                            @php
                                                $differenceA6 = $m_core->A6 - $nilaiA6;
                                                $nilaiBobotA6 = isset($nilaiBobotMapping[$differenceA6]) ? $nilaiBobotMapping[$differenceA6] : '';
                                                echo $nilaiBobotA6;
                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php

                                             $averageA4A6 = ($nilaiBobotA4 + $nilaiBobotA6) / 2;
                                             echo $averageA4A6;

                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php
                                                $avgA5 = $nilaiBobotA5/1;
                                                echo $avgA5;
                                            @endphp
                                        </td>
                                       
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse

                        
                  	</table>
                </div>


                {{-- NILAI TOTAL --}}


                <div class="table-responsive p-3" >
                    
                    <table class="table align-items-center  table-hover" border='1'>
                    	
                            <tr style="background: rgb(117, 247, 117);">
                                <td colspan="5" align="center">ASPEK AKADEMIK</td>
                            </tr>
                            <tr>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NO.</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NAMA</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">CF</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">SF</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NA</td>
                            </tr>
                            <tbody> 


                                 @php
                                 $no_bobot=1;

                                  $nilaiBobotMapping = [
                                        0 => 9,
                                        1 => 8,
                                        -1 => 7,
                                        2 => 6,
                                        -2 => 5,
                                        3 => 4,
                                        -3 => 3,
                                        4 => 2,
                                        -4 => 1,
                                    ];
                             @endphp
                           
                             @forelse ($mahasiswa_core as $m_core)
                                    <tr class="bg">
                                        <td align="center">{{$no_bobot++}}</td>
                                        <td align="center">
                                          {{$m_core->nama}}
                                        </td>
                                       
                                        <td align="center">
                                             @php
                                                $differenceA1 = $m_core->A1 - $nilaiA1;
                                                $nilaiBobotA1 = isset($nilaiBobotMapping[$differenceA1]) ? $nilaiBobotMapping[$differenceA1] : '';

                                                $differenceA2 = $m_core->A2 - $nilaiA2;
                                                $nilaiBobotA2 = isset($nilaiBobotMapping[$differenceA2]) ? $nilaiBobotMapping[$differenceA2] : '';

                                                $averageA1A2 = ($nilaiBobotA1 + $nilaiBobotA2) / 2;
                                                echo $averageA1A2;
                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php
                                                $differenceA3 = $m_core->A3 - $nilaiA3;
                                                $nilaiBobotA3 = isset($nilaiBobotMapping[$differenceA3]) ? $nilaiBobotMapping[$differenceA3] : '';
                                                echo $nilaiBobotA3;
                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php

                                            $CF = $averageA1A2;
                                            $SF = $nilaiBobotA3;

                                            $NA = ($CF * 0.6) + ($SF * 0.4);
                                            echo $NA;

                                            @endphp
                                        </td>
                                       
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse

                            

                            </tbody>
                        
                  	</table>
                </div>


                 <div class="table-responsive p-3" style="margin-top:-32px; ">
                    
                    <table class="table align-items-center  table-hover" border='1'>
                             <tr style="background: rgba(181, 181, 181, 0.852);">
                                <td colspan="5" align="center">Nilai Total</td>
                            </tr>

                            <tr style="background: rgb(117, 247, 117);">
                                <td colspan="5" align="center">ASPEK NON AKADEMIK</td>
                            </tr>
                            <tr>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NO.</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NAMA</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">CF</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">SF</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NN</td>
                            </tr>
                            <tbody> 


                                 @php
                                 $no_bobot=1;

                                  $nilaiBobotMapping = [
                                        0 => 9,
                                        1 => 8,
                                        -1 => 7,
                                        2 => 6,
                                        -2 => 5,
                                        3 => 4,
                                        -3 => 3,
                                        4 => 2,
                                        -4 => 1,
                                    ];
                             @endphp
                           
                             @forelse ($mahasiswa_core as $m_core)
                                    <tr class="bg">
                                        <td align="center">{{$no_bobot++}}</td>
                                        <td align="center">
                                          {{$m_core->nama}}
                                        </td>
                                       
                                        <td align="center">
                                             @php
                                                $differenceA4 = $m_core->A4 - $nilaiA4;
                                                $nilaiBobotA4 = isset($nilaiBobotMapping[$differenceA4]) ? $nilaiBobotMapping[$differenceA4] : '';

                                                $differenceA6 = $m_core->A6 - $nilaiA6;
                                                $nilaiBobotA6 = isset($nilaiBobotMapping[$differenceA6]) ? $nilaiBobotMapping[$differenceA6] : '';

                                                $averageA4A6 = ($nilaiBobotA4 + $nilaiBobotA6) / 2;
                                                echo $averageA4A6;
                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php
                                                $differenceA5 = $m_core->A5 - $nilaiA5;
                                                $nilaiBobotA5 = isset($nilaiBobotMapping[$differenceA5]) ? $nilaiBobotMapping[$differenceA5] : '';
                                                
                                                $avgA5 = $nilaiBobotA5/1;
                                                echo $avgA5;
                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php

                                                $CF = $averageA4A6;
                                                $SF = $avgA5;

                                                $NN = ($CF * 0.6) + ($SF * 0.4);
                                                echo $NN;

                                            @endphp
                                        </td>
                                       
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse

                            

                            </tbody>
                        
                  	</table>
                </div>


                <div class="table-responsive p-3">
                    
                    <table class="table align-items-center  table-hover" border='1'>
                             <tr style="background: rgba(181, 181, 181, 0.852);">
                                <td colspan="5" align="center">Perhitungan Ranking</td>
                            </tr>

                          
                            <tr>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NO.</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NAMA</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NA</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">NN</td>
                                <td rowspan="2" align="center" style="background: rgb(117, 247, 117);">HASIL AKHIR</td>
                            </tr>
                            <tbody> 


                                 @php
                                 $no_bobot=1;

                                  $nilaiBobotMapping = [
                                        0 => 9,
                                        1 => 8,
                                        -1 => 7,
                                        2 => 6,
                                        -2 => 5,
                                        3 => 4,
                                        -3 => 3,
                                        4 => 2,
                                        -4 => 1,
                                    ];
                             @endphp
                           
                             @forelse ($mahasiswa_core as $m_core)
                                    <tr class="bg">
                                        <td align="center">{{$no_bobot++}}</td>
                                        <td align="center">
                                          {{$m_core->nama}}
                                        </td>
                                       
                                        <td align="center">
                                             @php

                                                $differenceA1 = $m_core->A1 - $nilaiA1;
                                                $nilaiBobotA1 = isset($nilaiBobotMapping[$differenceA1]) ? $nilaiBobotMapping[$differenceA1] : '';

                                                $differenceA2 = $m_core->A2 - $nilaiA2;
                                                $nilaiBobotA2 = isset($nilaiBobotMapping[$differenceA2]) ? $nilaiBobotMapping[$differenceA2] : '';

                                                $averageA1A2 = ($nilaiBobotA1 + $nilaiBobotA2) / 2;


                                                $differenceA3 = $m_core->A3 - $nilaiA3;
                                                $nilaiBobotA3 = isset($nilaiBobotMapping[$differenceA3]) ? $nilaiBobotMapping[$differenceA3] : '';
                                                

                                                $CF = $averageA1A2;
                                                $SF = $nilaiBobotA3;

                                                $NA = ($CF * 0.6) + ($SF * 0.4);
                                                echo $NA;

                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php
                                                $differenceA4 = $m_core->A4 - $nilaiA4;
                                                $nilaiBobotA4 = isset($nilaiBobotMapping[$differenceA4]) ? $nilaiBobotMapping[$differenceA4] : '';

                                                $differenceA6 = $m_core->A6 - $nilaiA6;
                                                $nilaiBobotA6 = isset($nilaiBobotMapping[$differenceA6]) ? $nilaiBobotMapping[$differenceA6] : '';

                                                $averageA4A6 = ($nilaiBobotA4 + $nilaiBobotA6) / 2;


                                                $differenceA5 = $m_core->A5 - $nilaiA5;
                                                $nilaiBobotA5 = isset($nilaiBobotMapping[$differenceA5]) ? $nilaiBobotMapping[$differenceA5] : '';
                                                
                                                $avgA5 = $nilaiBobotA5/1;

                                                $CF = $averageA4A6;
                                                $SF = $avgA5;

                                                $NN = ($CF * 0.6) + ($SF * 0.4);
                                                echo $NN;

                                            @endphp
                                        </td>

                                         <td align="center">
                                            @php

                                            $NA_AKHIR = $NA;
                                            $NN_AKHIR = $NN;

                                            $HA = ($NA_AKHIR * 0.6) + ($NN_AKHIR * 0.4);
                                            echo $HA;

                                            @endphp
                                        </td>
                                       
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse

                            

                            </tbody>
                        
                  	</table>
                </div>

            </div>
        </div>
    </div>
</div>





<script>

    function edit(){
        alert("OOPS FITUR MASIH PROGRES, TERIMAKASIH");
    }


</script>

    

    
@endsection