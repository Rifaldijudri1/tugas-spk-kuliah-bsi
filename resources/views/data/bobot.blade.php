@extends('templates.header')
@push('style')
@endpush
@section('content')


<div class="container-fluid" id="container-wrapper">
	<div class="row mb-3">
		<div class="col-lg-12">
			<div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-white">Bobot</h6>
                    {{-- <div class="text-right">
                         @if(Auth::user()->roles == 'Super')
                        <a href="" class="btn btn-primary btn-sm no-hov" data-toggle="modal" data-target="#modalData"><i class="fa fa-plus"></i>&nbsp; Tambah</a>
                        @else
                        @endif
                    </div> --}}
                </div>
                <div class="table-responsive p-3">
                    
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                    	<thead class="thead-light">
                        	<tr>
                        	    <th width="5%">No</th>
                                <th>Selisih</th>
                                <th>Nilai</th>
                                <th>Keterangan</th>
                        	</tr>
                    	</thead>
                        @php
                            $no=1;
                        @endphp
                    	<tbody>
                        	 @forelse ($bobot as $bobot)
                                    <tr class="bg">
                                        <td>{{$no++}}</td>
                                        <td>
                                          {{$bobot->selisih}}
                                        </td>
                                        <td>
                                          {{$bobot->nilai}}
                                        </td>
                                        <td>
                                          {{$bobot->keterangan}}
                                        </td>
                                      
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse
                              
                        
                    	</tbody>
                  	</table>
                </div>
            </div>
        </div>
    </div>
</div>



    

  

@endsection