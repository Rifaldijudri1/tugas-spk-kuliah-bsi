<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="novelhub">
  <link rel="shortcut icon" href="{{asset('img/icon-logo.png')}}" type="image/x-icon" />
	<link rel="apple-touch-icon" href="{{asset('img/icon-logo.png')}}">
  <title>Tugas SPK - Pertemuan 10</title>
  <link href="{{ asset('temp/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{ asset('temp/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{ asset('temp/css/xx-ruang-admin.min.css')}}" rel="stylesheet">

  <style type="text/css">

    .bg-g-app-primary {
      background: #005490 !important;
    }

    .bg-g-app-primary:hover {
      background: #1c3682  !important;
    }

    .border-none {
      border: 0px !important;
    }

    .logo {
      width: 200px;
      height: 100px;
      margin-top: 20px;
      margin-left: 35px
    }

    .b-bottom-l {
      width: 100%; 
      height: 4px; 
      background: #005490;
    }

    .image {
      width: 360px;
      height: 280px
    }

    @media (max-width: 1199px){
      .container-login {
        margin-left: 10rem;
        margin-right: 10rem;
      }
    }

    @media (max-width: 678px){
      .container-login {
        margin-left: 0rem;
        margin-right: 0rem;
      }
    }

    @media screen and (max-width: 991px) {
      .logo {
          margin-left: 0px
      }

      .image {
          width: 300px;
          height: 220px
      }
    }

  </style>

</head>

<body class="bg-gradient-login">

  <div class="p-3">
    <div class="row d-flex">
      <div class="col-lg-6 col-md-6">
        <div class="pb-5">
          <!-- <div class="row"> <img src=".." class="logo"> </div> -->
          <div class="row">
            <div class="col-xl-12 col-lg-12 text-center">
              {{-- <img src="{{ asset('img/Kementerian PPN Bappenas Square EN.png')}}" class="img-fluid" style="width: 75%"> --}}
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="card border-none py-4" style="height: calc(100vh - 30px);">
          <div class="px-4 mb-4">
            <div class="row">
              <div class="col-xl-12 col-lg-12">
                <h2 style="color:#005490; ">Tugas SPK - Pertemuan 10</h2>
              </div>
            </div>
          </div>
          <div class="b-bottom-l"></div>
          <div class="px-4 mt-4">
          
            <form class="user" action="{{ route('loginaksi') }}" method="post">
              @csrf
              <div class="form-group">
                <label>Username</label>
                <input type="text" class="form-control" autocomplete="off" name="username" placeholder="" autocomplete="off" value="">
              
              </div>

              <div class="form-group mb-3">
                <div class="input-group">
                  <input type="password" class="form-control" id="myInput" autocomplete="off" name="password" placeholder="*****************">
                  <div class="input-group-append" onclick="myFunction()">
                    <span class="input-group-text bg-g-app-primary border-none" id="basic-addon2" style="cursor: pointer;">
                      <i id="matanyaganti" class="fas fa-eye-slash"></i>
                    </span>
                  </div>
                </div>
               
              </div>
 
              <div class="form-group">
                <div class="custom-control custom-checkbox small" style="line-height: 1.5rem;">
                  <input type="checkbox" class="custom-control-input" id="customCheck">
                  <label class="custom-control-label" for="customCheck">Remember me</label>
                </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block bg-g-app-primary border-none">Login</button>
              </div>
            </form>
                @if (session('error'))
                    <div class="alert alert-danger">
                        <b>Opps!</b> {{ session('error') }}
                    </div>
                @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Login Content -->
  <script src="{{ asset('temp/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ asset('temp/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{ asset('temp/vendor/jquery-easing/jquery.easing.min.js')}}"></script>
  <script src="{{ asset('temp/js/ruang-admin.min.js')}}"></script>

  <script type="text/javascript">
    function myFunction() {
      var x = document.getElementById("myInput");
      if (x.type === "password") {
        x.type = "text";
        $('#matanyaganti').addClass('fa-eye');
        $('#matanyaganti').removeClass('fa-eye-slash');
      } else {
        $('#matanyaganti').addClass('fa-eye-slash');
        $('#matanyaganti').removeClass('fa-eye');
        x.type = "password";
      }
    }
  </script>
</body>

</html>