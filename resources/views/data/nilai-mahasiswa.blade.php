@extends('templates.header')
@push('style')
@endpush
@section('content')


<div class="container-fluid" id="container-wrapper">
	<div class="row mb-3">
		<div class="col-lg-12">
			<div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-white">Nilai Mahasiswa</h6>
                    <div class="text-right">
                         @if(Auth::user()->role == 'Super')
                        <a href="" class="btn btn-primary btn-sm no-hov" data-toggle="modal" data-target="#modalData"><i class="fa fa-plus"></i>&nbsp; Tambah Nilai</a>
                        @else
                        @endif
                    </div>
                </div>
                <div class="table-responsive p-3">
                    
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                    	<thead class="thead-light">
                        	<tr>
                        	    <th width="5%">No</th>
                                <th>Nama</th>
                                <th>Nim</th>
                                <th>Kelas</th>
                                <th>A1</th>
                                <th>A2</th>
                                <th>A3</th>
                                <th>A4</th>
                                <th>A5</th>
                                <th>A6</th>
                                <th>&nbsp;</th>
                        	</tr>
                    	</thead>
                        @php
                            $no=1;
                        @endphp
                    	<tbody>
                        	 @forelse ($mahasiswa as $mahasiswa)
                                    <tr class="bg">
                                        <td>{{$no++}}</td>
                                        <td>
                                          {{$mahasiswa->nama}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->nim}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->kelas}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A1}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A2}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A3}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A4}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A5}}
                                        </td>
                                        <td>
                                          {{$mahasiswa->A6}}
                                        </td>
                                        
                                        <td>
                                            <a href="#" onclick="edit()"  class="btn btn-warning btn-sm font-size-12 mb-1">Edit</a>
                                        </td>
                                    
                                        
                                       
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse
                              
                        
                    	</tbody>
                  	</table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Nilai Mahasiswa</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="tambahform" action="{{ route('save-mahasiswa') }}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                     @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nama Mahasiswa<span style="color: red">*</span></label>
                                <input type="text" class="form-control  @error('nama') is-invalid @enderror" name="nama" required="" autocomplete="off">
                                 @error('nama')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                         <div class="col-md-12">
                            <div class="form-group">
                                <label>Nim<span style="color: red">*</span></label>
                                <input type="text" class="form-control  @error('nim') is-invalid @enderror" name="nim" required="" autocomplete="off">
                                 @error('nim')
                                    <div class="alert alert-danger mt-2">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Kelas<span style="color: red">*</span></label>
                                <select class="form-control selectpicker" name="kelas" data-live-search="true">
                                    <option>-Pilih kelas-</option>
                                    <option value="19.7AA.07">19.7AA.07</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nilai A1<span style="color: red">*</span></label>
                                <input type="text" class="form-control  " name="A1" placeholder="" value="" autocomplete="off">
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Nilai A2<span style="color: red">*</span></label>
                                <input type="text" class="form-control  " name="A2" placeholder="" value="" autocomplete="off">
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Nilai A3<span style="color: red">*</span></label>
                                <input type="text" class="form-control  " name="A3" placeholder="" value="" autocomplete="off">
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Nilai A4<span style="color: red">*</span></label>
                                <input type="text" class="form-control  " name="A4" placeholder="" value="" autocomplete="off">
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Nilai A5<span style="color: red">*</span></label>
                                <input type="text" class="form-control  " name="A5" placeholder="" value="" autocomplete="off">
                            </div>
                        </div>

                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Nilai A6<span style="color: red">*</span></label>
                                <input type="text" class="form-control  " name="A6" placeholder="" value="" autocomplete="off">
                            </div>
                        </div>




                    </div>

                    

                    
                        
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">&nbsp;Submit&nbsp;</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>

    function edit(){
        alert("OOPS FITUR MASIH PROGRES, TERIMAKASIH");
    }


</script>

    

    
@endsection