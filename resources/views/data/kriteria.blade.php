@extends('templates.header')
@push('style')
@endpush
@section('content')


<div class="container-fluid" id="container-wrapper">
	<div class="row mb-3">
		<div class="col-lg-12">
			<div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-white">Kriteria</h6>
                    {{-- <div class="text-right">
                         @if(Auth::user()->roles == 'Super')
                        <a href="" class="btn btn-primary btn-sm no-hov" data-toggle="modal" data-target="#modalData"><i class="fa fa-plus"></i>&nbsp; Tambah</a>
                        @else
                        @endif
                    </div> --}}
                </div>
                <div class="table-responsive p-3">
                    
                    <table class="table align-items-center table-flush table-hover" id="dataTable">
                    	<thead class="thead-light">
                        	<tr>
                        	    <th width="5%">No</th>
                                <th>Nama</th>
                                <th>Status</th>
                                
                        	</tr>
                    	</thead>
                        @php
                            $no=1;
                        @endphp
                    	<tbody>
                        	 @forelse ($kriteria as $kriteria)
                                    <tr class="bg">
                                        <td>{{$no++}}</td>
                                        {{-- <td>{{$kriteria->nama}}</td> --}}
                                        <td>
                                          {{$kriteria->nama}}
                                        </td>
                                        
                                        
                                        @if($kriteria->status == 'Active')
                                        <td><span class="badge badge-success">{{$kriteria->status}}</span></td>
                                        @else
                                        <td><span class="badge badge-danger">{{$kriteria->status}}</span></td>  
                                        @endif
                                        
                                       
                                    </tr>
                               @empty
                                  <div class="alert alert-danger">
                                      Data user belum Tersedia.
                                  </div>
                               @endforelse
                              
                        
                    	</tbody>
                  	</table>
                </div>
            </div>
        </div>
    </div>
</div>



    

     <script>
        $(document).ready(function() {
            toastr.options.timeOut = 5000;
            @if (Session::has('error'))
                toastr.error('{{ Session::get('error') }}');
            @elseif(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif

            
        });

        function previewImage(){
            const image = document.querySelector('#gambar');
            const imagePreview = document.querySelector('.img-preview');


            imagePreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent){
                imagePreview.src = oFREvent.target.result;

            }
        }

    </script>

@endsection