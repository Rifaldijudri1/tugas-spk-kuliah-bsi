      </div>
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script> - powered by
              <b><a href="javascript:">Rifaldi Judri, Riko Gatot, Rosidah</a></b>
            </span>
          </div>
        </div>
      </footer>
      <!-- Footer -->
    </div>
  </div>

  <style type="text/css">
    .full_modal-dialog {
      width: 98% !important;
      height: 92% !important;
      min-width: 98% !important;
      min-height: 92% !important;
      max-width: 98% !important;
      max-height: 92% !important;
      padding: 0 !important;
    }

    /*.full_modal-content {
      height: 99% !important;
      min-height: 99% !important;
      max-height: 99% !important;
    }*/
    .css-calender-input{
      background: #353535;
      padding: 7px;
      border-radius: 0px 3px 3px 0px;
      color: white;
      font-size: 10pt;
    }
  </style>

  <!-- Scroll to top -->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <div class="modal fade" id="myModalnormal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="optionnormal"><span style="padding: 15px;">Loading...</span></div>
    </div>
  </div>

  <div class="modal fade" id="myModalnormal2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="optionnormal2"><span style="padding: 15px;">Loading...</span></div>
    </div>
  </div>

  <div class="modal fade" id="myModalnormal3" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="optionnormal3"><span style="padding: 15px;">Loading...</span></div>
    </div>
  </div>

  <div class="modal fade" id="myModalsedang" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="optionsedang"><span style="padding: 15px;">Loading...</span></div>
    </div>
  </div>

  <div class="modal fade" id="myModalsedang2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="optionsedang2"><span style="padding: 15px;">Loading...</span></div>
    </div>
  </div>

  <div class="modal fade" id="myModalbesar" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content " id="optionbesar"><span style="padding: 15px;">Loading...</span></div>
    </div>
  </div>

  <div class="modal fade" id="myModalfull" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog full_modal-dialog" role="document">
        <div class="modal-content " id="optionfull"><span style="padding: 15px;">Loading...</span></div>
    </div>
  </div>

  <div class="modal fade" id="myModalfull2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog full_modal-dialog" role="document">
        <div class="modal-content " id="optionfull2"><span style="padding: 15px;">Loading...</span></div>
    </div>
  </div>
  <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
  {{-- <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script> --}}
  <script src="{{asset('temp/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('temp/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('temp/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <script src="{{asset('temp/vendor/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('temp/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('temp/vendor/datepicker-1.9.0/js/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('temp/vendor/tinymce/tinymce/tinymce.min.js')}}"></script>

  <script src="{{asset('temp/js/ruang-admin.min.js')}}"></script>

  <script src="{{asset('temp/vendor/bootstrap-select.min.js')}}"></script>
  <script src="{{asset('temp/vendor/jquery.fancybox.min.js')}}"></script>
  <script src="{{asset('temp/vendor/jquery-confirm.min.js')}}"></script>

  <script src="{{asset('js/moment.min.js')}}"></script>
  <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>

      {{-- toastr js --}}
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
    <script>
    //    ClassicEditor.create( document.querySelector( '#desc' ) )
    //    ClassicEditor.create( document.querySelector( '#desc2' ) )
    //    ClassicEditor.create( document.querySelector( '#desc3' ) )
    //    ClassicEditor.create( document.querySelector( '#desc4' ) )
    //    ClassicEditor.create( document.querySelector( '#desc5' ) )
    //    ClassicEditor.create( document.querySelector( '#desc6' ) )
    // .catch( error => {
    //     console.error( error );
    //  } );


    // $("#Ujikom_sub").hide();
    
    //     function setKategori() {
    //         var x = document.getElementById("kategori").value;
    //         if(x == "Ujikom"){
    //             $("#Ujikom_sub").show();
    //         }else{
    //             $("#Ujikom_sub").hide();
    //         }
    //     }

    //MODAL SHOW
       
        $(document).ready(function(){
					$("#userShowInput").modal('show');
          $('[data-toggle="tooltip"]').tooltip()
         // $('#datetimepicker1').datetimepicker();
          $('#datetimepicker1').datetimepicker({
           
            format: 'DD/MM/YYYY hh:mm A',
          
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-check',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
        });
				});
    </script>

    <script>
        $(document).ready(function() {
            toastr.options.timeOut = 5000;
            @if (Session::has('error'))
                toastr.error('{{ Session::get('error') }}');
            @elseif(Session::has('success'))
                toastr.success('{{ Session::get('success') }}');
            @endif
        });

        $('#dataTable').DataTable({
          ordering: false,
          "lengthMenu": [[25, 100, 150, -1], [25, 100, 150, "All"]]
        }); 

          $('#dataTable2').DataTable({
          ordering: false,
          "lengthMenu": [[25, 100, 150, -1], [25, 100, 150, "All"]]
        }); 
        

    </script>
    {{-- Script modal card di dashboard helpdesk TAMBAHAN DIO --}}
    <script>
      $(document).on('click', '.close', function() {
         $(this).closest('#modal-hello').fadeOut('slow');
      });
  </script>

<script>
$(document).ready(function() {
  $('a.nav-link.keluar').click(function(event) {
    event.preventDefault();
    var url = $(this).attr('href');
    if (confirm('Anda yakin ingin keluar?')) {
      window.location.href = url;
    }
  });
});
</script>

@stack('scripts')


  
 
 

</body>

</html>