

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  {{-- TAMBAHKAN DIO --}}
 
      <title>Pusbindiklatren</title>

  <link rel="shortcut icon" href="{{asset('img/icon-logo.png')}}" type="image/x-icon" />
	<link rel="apple-touch-icon" href="{{asset('img/icon-logo.png')}}">
  <link href="{{asset('temp/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('temp/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('temp/css/xx-ruang-admin.min.css')}}" rel="stylesheet">
  <link href="{{asset('temp/css/xx-styles.css')}}" rel="stylesheet">
  <link href="{{asset('temp/css/style.css')}}" rel="stylesheet">
  <link href="{{asset('temp/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
  <link href="{{asset('temp/vendor/boostrap-select.min.css')}}" rel="stylesheet">
  <link href="{{asset('temp/vendor/datepicker-1.9.0/css/bootstrap-datepicker3.css')}}" rel="stylesheet">
  <link href="{{asset('temp/vendor/bootstrap-select.min.css')}}" rel="stylesheet">
  <link href="{{asset('temp/vendor/jquery.fancybox.min.css')}}" media="screen" rel="stylesheet">
  <link href="{{asset('temp/vendor/jquery-confirm.min.css')}}" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-datetimepicker.css')}}">

  	<!-- Chats CSS -->
		<link rel="stylesheet" href="{{ asset('css/chats.css')}}">

   {{-- toastr --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />

  
  <style type="text/css">

  .padding-submit {
    padding: 5px 0px 20px 20px !important;
  }

  .padding-submit-2 {
    padding: 5px 0px 5px 20px !important;
  }

  i.blink, span.blink { animation: blink 1s linear infinite; }
  @keyframes blink{
    0% { opacity: 0; }
    50% { opacity: .5; }
    100% { opacity: 1; }
  }
/* 
  input[type="date"]::-webkit-inner-spin-button,
  input[type="date"]::-webkit-calendar-picker-indicator {
      display: none;
      -webkit-appearance: none;
  } */

  .vertical-align-middle {
    vertical-align: middle !important;
  }

  .btn-xs {
    padding: 1px 5px !important;
    font-size: 12px !important;
    line-height: 1.5 !important;
    border-radius: 3px !important;
  }

   /* TAMBAHKAN DIO */
   ol.breadcrumb {
    font-size: 15px;
  }

  </style>

</head>

<body id="page-top">
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#" style="{{ Auth::user()->role=="Admin_help" ? 'background-color:#154c79;' : '' }}">
        <div class="sidebar-brand-icon">
          {{-- <img src="{{ asset('img/icon-logo.png')}}"> --}}
        </div>
        {{-- modifikasi sidebar DIO --}}
      
        <div class="sidebar-brand-text mx-3">Tugas SPK - Pertemuan 10</div>
      
      </a>
      <hr class="sidebar-divider my-0">
      @if(Auth::user()->role == 'Super' )
          <li class="nav-item {{ Request::is('kriteria') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('kriteria')}}">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Kriteria</span>
            </a>
          </li>

          <li class="nav-item {{ Request::is('sub-kriteria') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('sub-kriteria')}}">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Sub Kriteria</span>
            </a>
          </li>

          <li class="nav-item {{ Request::is('bobot') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('bobot')}}">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Bobot</span>
            </a>
          </li>

          <li class="nav-item {{ Request::is('nilai-mahasiswa') || Request::is('nilai-hitung-mahasiswa') ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBootstrap"
              aria-expanded="true" aria-controls="collapseBootstrap">
              <i class="fas fa-fw fa-tachometer-alt"></i>
              <span>Mahasiswa</span>
            </a>
            <div id="collapseBootstrap" class="collapse " aria-labelledby="headingBootstrap" data-parent="#accordionSidebar" style="background: #d9d9d9 !important;">
              <div class="py-2 collapse-inner rounded" style="background: #d9d9d9 !important;">
              
                <a class="collapse-item {{ Request::is('nilai-mahasiswa')  ? 'active' : '' }}"  href="{{route('nilai-mahasiswa')}}">Nilai Mahasiswa</a>
              
              </div>

              <div class="py-2 collapse-inner rounded" style="background: #d9d9d9 !important;">
              
                <a class="collapse-item {{ Request::is('nilai-hitung-mahasiswa')  ? 'active' : '' }}"  href="{{route('nilai-hitung')}}">Perhitungan Nilai</a>
              
              </div>

            </div>
          </li>

       @endif
     
      <hr class="sidebar-divider">
      <div class="version margin-bot-15" id="version-ruangadmin"></div>
    </ul>
    <!-- Sidebar -->
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
         @include('templates.topbar')
         @yield('content')
         @yield('scripts')
         @include('templates.footer')


        
