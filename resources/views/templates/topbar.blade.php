
@php
use App\Models\Tiket;
use Carbon\Carbon;
@endphp

{{-- INI JUGA --}}
<nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top" style="{{ Auth::user()->role == "Admin_help" ? 'background-color:#0b3455;' : '' }}">
  <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3 d-block d-sm-none d-none d-sm-block d-md-none">
    <i class="fa fa-bars"></i>
  </button>
  <ul class="navbar-nav ml-auto">

 
    <div class="topbar-divider d-none d-sm-block"></div>

    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img class="img-profile rounded-circle" src="{{asset('temp/img/boy.png')}}" style="max-width: 60px">
        <span class="ml-2 d-none d-lg-inline text-white small">{{ Auth::user()->nama_lengkap }}</span>
      </a>
      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
        <a class="dropdown-item" href="#" data-toggle="modal" data-target="#modalDataProfil">
          <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
          Profil
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item " href="{{route('logoutaksi')}}">
          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
          Keluar
        </a>
      </div>
    </li>
  
  </ul>
</nav>

<div class="modal fade" id="modalDataProfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Data Profil</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editformprofilglob" action="#" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden"  name="uid" value="{{ Auth::user()->uid }}" >
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama Lengkap<span style="color: red">*</span></label>
                        <input type="text" class="form-control" name="nama_lengkap" required="" value="{{ Auth::user()->nama }}" autocomplete="off">
                      
                    </div>
                    <div class="form-group">
                        <label>Email<span style="color: red">*</span></label>
                        <input type="email" class="form-control" name="name" value="{{ Auth::user()->email }}" disabled autocomplete="off">
                      
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" placeholder="*************" autocomplete="off" >
                        <small class="form-text text-muted">Kosongkan jika tidak dirubah.</small>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary">&nbsp;Submit&nbsp;</button>
                </div>
            </form>
        </div>
    </div>
</div>