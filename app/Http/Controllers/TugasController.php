<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bobot;
use App\Models\Kriteria;
use App\Models\SubKriteria;
use App\Models\Mahasiswa;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

use DB;

class TugasController extends Controller
{
    
    public function index()
    {      
        $kriteria = Kriteria::where('status','Active')->get();
        return view('data.kriteria', [
           'kriteria' => $kriteria,
        ]);
    }

    public function sub_kriteria()
    {      
        $sub_kriteria = Subkriteria::with('kriteria')->where('status', 'Active')->get();

       // dd($sub_kriteria);

        return view('data.sub-kriteria', [
           'sub_kriteria' => $sub_kriteria,
        ]);
    }

    public function bobot()
    {      
        $bobot = Bobot::orderBy('created_at','asc')->get();
        return view('data.bobot', [
           'bobot' => $bobot,
        ]);
    }

    public function nilai_mahasiswa()
    {      
        $mahasiswa = Mahasiswa::orderBy('created_at','asc')->get();
        return view('data.nilai-mahasiswa', [
           'mahasiswa' => $mahasiswa,
        ]);
    }

     public function save_mahasiswa(Request $request){      
       
        $mahasiswa = new Mahasiswa;

        $mahasiswa->uid = (string) Str::uuid();
        $mahasiswa->nama = $request->nama;
        $mahasiswa->nim = $request->nim;
        $mahasiswa->kelas = $request->kelas;
        $mahasiswa->A1 = $request->A1;
        $mahasiswa->A2 = $request->A2;
        $mahasiswa->A3 = $request->A3;
        $mahasiswa->A4 = $request->A4;
        $mahasiswa->A5 = $request->A5;
        $mahasiswa->A6 = $request->A6;

        $mahasiswa->save();

         if($mahasiswa){
            return redirect()->route('nilai-mahasiswa')->with(['success' => 'Data Berhasil Disimpan!']);
        }else{
            //redirect dengan pesan error
            return redirect()->route('nilai-mahasiswa')->with(['error' => 'Data Gagal Disimpan!']);
        }
        
    }

     public function nilai_hitung()
    {      
        $mahasiswa = Mahasiswa::orderBy('created_at','asc')->get();
        $mahasiswa2 = Mahasiswa::orderBy('created_at','asc')->get();
        $mahasiswa_bobot = Mahasiswa::orderBy('created_at','asc')->get();
        $mahasiswa_bobot2 = Mahasiswa::orderBy('created_at','asc')->get();
        $mahasiswa_core = Mahasiswa::orderBy('created_at','asc')->get();

        $bobot = Bobot::orderBy('created_at','asc')->get();
        
        $nilai_standar = Subkriteria::orderBy('created_at','asc')->get();
        
        $nilaiA1 = $nilai_standar->where('kode', 'A1')->first()->nilai;
        $nilaiA2 = $nilai_standar->where('kode', 'A2')->first()->nilai;
        $nilaiA3 = $nilai_standar->where('kode', 'A3')->first()->nilai;
        $nilaiA4 = $nilai_standar->where('kode', 'A4')->first()->nilai;
        $nilaiA5 = $nilai_standar->where('kode', 'A5')->first()->nilai;
        $nilaiA6 = $nilai_standar->where('kode', 'A6')->first()->nilai;

        return view('data.nilai-hitung', [
           'mahasiswa' => $mahasiswa,
           'mahasiswa2' => $mahasiswa2,
           'mahasiswa_bobot' => $mahasiswa_bobot,
           'mahasiswa_bobot2' => $mahasiswa_bobot2,
           'mahasiswa_core' => $mahasiswa_core,
           'bobot' => $bobot,
           'nilai_standar' => $nilai_standar,
           'nilaiA1' => $nilaiA1,
           'nilaiA2' => $nilaiA2,
           'nilaiA3' => $nilaiA3,
           'nilaiA4' => $nilaiA4,
           'nilaiA5' => $nilaiA5,
           'nilaiA6' => $nilaiA6,
        ]);
    }


    

}