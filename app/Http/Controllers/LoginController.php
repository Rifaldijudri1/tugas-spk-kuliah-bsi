<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


use Session;

class LoginController extends Controller
{
    
    public function login()
    {
        if (Auth::check()) {
            if (Auth::user()->role == 'Super') {
                return redirect('kriteria');
            } else {
               // return redirect('e-formasi/dashboard/user');
            }
        } else {
            return view('data.login');
        }
    }

    public function loginaksi(Request $request)
    {
        $data = [
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'status' => 'Active'
        ];

        if (Auth::Attempt($data)) {

            if (Auth::user()->role == 'Super') {
                 return redirect('kriteria');
            }  else {
            //    return redirect('e-formasi/dashboard/user');
            }
        } else {

            Session::flash('error', 'Username atau Password Salah');
            return redirect('/login');
        }
    }

    public function logoutaksi()
    {
        Auth::logout();
        return redirect('/login');
    }
}
