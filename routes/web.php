<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
});


Route::get('/login', [App\Http\Controllers\LoginController::class, 'login'])->name('login');
Route::post('loginaksi', [App\Http\Controllers\LoginController::class, 'loginaksi'])->name('loginaksi');
Route::get('logoutaksi', [App\Http\Controllers\LoginController::class, 'logoutaksi'])->name('logoutaksi')->middleware('auth');


Route::get('/kriteria', [App\Http\Controllers\TugasController::class, 'index'])->name('kriteria');
Route::get('/sub-kriteria', [App\Http\Controllers\TugasController::class, 'sub_kriteria'])->name('sub-kriteria');
Route::get('/bobot', [App\Http\Controllers\TugasController::class, 'bobot'])->name('bobot');

Route::get('/nilai-mahasiswa', [App\Http\Controllers\TugasController::class, 'nilai_mahasiswa'])->name('nilai-mahasiswa');
Route::post('/nilai-mahasiswa/save', [App\Http\Controllers\TugasController::class, 'save_mahasiswa'])->name('save-mahasiswa');
Route::get('/nilai-hitung-mahasiswa', [App\Http\Controllers\TugasController::class, 'nilai_hitung'])->name('nilai-hitung');